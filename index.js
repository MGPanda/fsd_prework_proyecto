function verExperiencia() {
  var element = document.getElementById("experience");
  if (element.classList.contains("active")) {
    document.getElementById("experience").classList.remove("active");
  } else {
    element.classList.add("active");
    document.getElementById("studies").classList.remove("active");
  }
}

function verEstudios() {
  var element = document.getElementById("studies");
  if (element.classList.contains("active")) {
    document.getElementById("studies").classList.remove("active");
  } else {
    element.classList.add("active");
    document.getElementById("experience").classList.remove("active");
  }
}

function imprimirNombre() {
    var myName = document.getElementById("name").innerHTML;
    for (i = 0; i < 100; i++) {
        console.log(myName);
    }
}